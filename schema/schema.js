const graphql = require ('graphql');
const { GraphQLObjectType, GraphQLString, GraphQLSchema,GraphQLID , GraphQLInt ,  GraphQLList} = graphql;
const _ = require('lodash');

const book = require("../models/book");
const Author = require("../models/Author");

var books = [
  { name : "hello" , genre : "fgghh" , id :"1" , authors_id:'1'  },
  { name : "hello" , genre : "fgghh" , id :"2" , authors_id:'2'},
  { name : "hello" , genre : "fgghh" , id :"3" , authors_id:'1'},  
  { name : "hello" , genre : "fgghh" , id :"4" , authors_id:'2'},
  { name : "hello" , genre : "fgghh" , id :"5" , authors_id:'1'},
];

var authors = [
  { name : "hello" ,age : 45 , id :'1' },
  { name : "hello" , age: 45 , id :'2' },
  { name : "hello" ,age : 45 , id :'3' },  
];


const BookType = new GraphQLObjectType({
	name :'book',
	fields:()=>({
		id: { type: GraphQLID},
		name : { type: GraphQLString} , 
		genre : { type: GraphQLString} , 
		author: {
			type : AuthorType,
			resolve(parent , args ){
				console.log(parent);
				//return _.find( authors , {id: parent.authors_id });
			}
		}
	})
});

const AuthorType = new GraphQLObjectType({
	name :'Author',
	fields:()=>({
		id: { type: GraphQLID},
		name : { type: GraphQLString} , 
		age : { type: GraphQLInt} ,
		books: {
			type : new  GraphQLList( BookType),
			resolve(parent , args ){
				return _.filter( books , {authors_id: parent.id });
			}
		}
	})
});

/*requeries  how to get into the data*/
const RootQuery = new GraphQLObjectType({
	name :'RootQueryType',
	fields:{
			book:{
				type:BookType,
				args:{id:{type:GraphQLID}},
				resolve(parent,args){
					console.log(typeof(args));
					//code get data from database
					//return _.find(books , { id: args.id});
				}
			}, 
			author :{
				type: AuthorType ,
				args : {id : {type:GraphQLID}},
				resolve(parent , args){
					//return _.find(authors , { id : args.id});
				}
			} ,

			books : {
				type: new GraphQLList(BookType),
				resolve(parent, args)
				{
					//return books ;
				}

			},
			authors : {
				type: new GraphQLList(AuthorType),
				resolve(parent, args)
				{
					//return authors ;
				}

			}

		}
});

const Mutation = new GraphQLObjectType({
	name:'Mutation',
	fields:{
		addauthor:{
			type:AuthorType,
			args:{
				name:{type:GraphQLString},
				age:{type:GraphQLInt}
			},
			resolve(parent , args){
				console.log("hello");
				let author = new Author({
					name:args.name,
					age:args.age
				});
				console.log("hello");
                 
				return author.save();

			}
		},
		addBook:{
			type: BookType ,
			args:{
				name:{type:GraphQLString},
				genre:{type:GraphQLString},
				authorId:{type:GraphQLID}
			},
			resolve(parent , args){
				console.log("hello");
				let Book = new book({
					name:args.name,
					genre:args.genre,
					authorId:args.authorId
				});
				console.log("hello");
                 
				return Book.save();

			}
		}
	}
})
module.exports = new GraphQLSchema({
	query: RootQuery, 
	mutation: Mutation 
});
