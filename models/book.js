const mongoose = require('mongoose');
const Schema = mongoose.Schema ;

const BookSchema = new Schema ({
name: String ,
genre: String ,
author_id :Number
});

module.exports = mongoose.model('book' ,BookSchema ) ;